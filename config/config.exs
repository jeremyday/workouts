# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :workouts,
  workouts_file: "../workouts.json"

config :logger,
       backends: [
         :console,
         { LoggerFileBackend, :error_log },
         { LoggerFileBackend, :debug_log }
       ]

config :logger,
       :console,
       format:   "$date $time [$level][$metadata] $levelpad$message\n",
       metadata: [:application, :module, :function]

config :logger,
       :error_log,
       format:   "$date $time [$level][$metadata] $levelpad$message\n",
       level:    :error,
       metadata: [:application, :module, :function],
       path:     "log/error.log"

config :logger,
       :debug_log,
       format:   "$date $time [$level][$metadata] $levelpad$message\n",
       level:    :debug,
       metadata: [:application, :module, :function],
       path:     "log/debug.log"

# This configuration is loaded before any dependency and is restricted
# to this project. If another project depends on this project, this
# file won't be loaded nor affect the parent project. For this reason,
# if you want to provide default values for your application for
# 3rd-party users, it should be done in your "mix.exs" file.

# You can configure for your application as:
#
#     config :workouts, key: :value
#
# And access this configuration in your application as:
#
#     Application.get_env(:workouts, :key)
#
# Or configure a 3rd-party app:
#
#     config :logger, level: :info
#

# It is also possible to import configuration files, relative to this
# directory. For example, you can emulate configuration per environment
# by uncommenting the line below and defining dev.exs, test.exs and such.
# Configuration from the imported file will override the ones defined
# here (which is why it is important to import them last).
#
#     import_config "#{Mix.env}.exs"
