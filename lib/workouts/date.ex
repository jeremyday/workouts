defmodule Workouts.Date do
  def date_from_string(date_string) do
    Timex.parse! date_string, "%Y.%m.%d", :strftime
  end

  def month_string(date_string), do: range_string date_string, &Timex.beginning_of_month/1, &Timex.end_of_month/1

  def string_from_date(date) do
    Timex.format! date, "%Y.%m.%d", :strftime
  end

  def week_string(date_string), do: range_string date_string, &Timex.beginning_of_week/1, &Timex.end_of_week/1

  defp range_string(date_string, start_func, end_func) do
    date        = date_from_string date_string
    range_start = start_func.(date)
    range_end   = end_func.(date)

    "#{string_from_date(range_start)}-#{string_from_date(range_end)}"
  end
end
