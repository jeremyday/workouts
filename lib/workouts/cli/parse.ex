defmodule Workouts.CLI.Parse do
  @doc """
  Parse the command line arguments.

  `argv` - The command line arguments.
           "-h" or "--help" returns [:help].
           "test" returns [:test].

  Returns an array of atoms based on `argv`.
  """
  def parse_args(argv) do
    switches    = [
      date:     :string,
      duration: :string,
      gain:     :integer,
      help:     :boolean,
      notes:    :string,
      race:     :string,
      shoes:    :string
    ]
    aliases     = [
      d:        :duration,
      g:        :gain,
      h:        :help,
      n:        :notes,
      r:        :race,
      s:        :shoes
    ]
    # parse contains { flags, commands, errors }
    # E.g., { [help: true], ["run"], [] }
    parse = OptionParser.parse List.flatten([argv]),
                               switches: switches,
                               aliases:  aliases

    case parse_flags parse do
      { :help } -> { :help }
      _         -> parse_commands parse
    end
  end

  defp current_date_string do
    { { year, month, day }, _ } = :calendar.local_time

    "#{year}.#{month}.#{day}"
  end

  defp parse_commands(parameters = { _, commands, _ }) do
    parse_commands commands, parameters
  end

  defp parse_commands([ head | tail ], parameters) do
    case parse_commands head, parameters do
      :help -> { :help }
      { :run, properties } -> { :run, properties }
      _     -> parse_commands tail, parameters
    end
  end

  # Catch the case when only the "run" command and no distance is given.
  defp parse_commands("run", { _, [command], _ }) do
    { :help }
  end

  # Catch the case when a "run" command and at least one other command is given.
  defp parse_commands("run", { flags, commands, _ }) do
    index    = Enum.find_index(commands, &(&1 == "run"))
    # TODO: Use todays' date as the default date.
    date     = Keyword.get flags, :date, current_date_string
    distance = String.to_float Enum.at(commands, index + 1,  0)
    duration = Keyword.get flags, :duration, ''
    gain     = Keyword.get flags, :gain,     0
    notes    = Keyword.get flags, :notes,    ''
    race     = Keyword.get flags, :race,     ''
    shoes    = Keyword.get flags, :shoes,    ''

    { :run, %{ date:     date,
               distance: distance,
               duration: duration,
               gain:     gain,
               notes:    notes,
               race:     race,
               shoes:    shoes } }
  end

  defp parse_commands(_, parameters) do
  end

  defp parse_flags(parameters = { flags, _, _ }) do
    parse_flags flags, parameters
  end

  defp parse_flags([ head | tail ], parameters) do
    case parse_flags head, parameters do
      :help -> { :help }
      _     -> parse_flags tail, parameters
    end
  end

  defp parse_flags({ :help, true }, parameters) do
    :help
  end

  defp parse_flags(_, parameters) do
  end
end
