defmodule Workouts.CLI.Process do
  def process_commands(command = { :help }) do
    IO.puts """
    usage:
      workouts run <distance> [--date=DATE --duration=DURATION --gain=GAIN --notes=NOTES --shoes=SHOES --race=RACE]
      workouts --help

    --date     date of the run in YYYY.MM.DD format
    --duration duration in HH:MM:SS of a run
    --gain     amount of climbing in feet during a run
    --help     show this output and exit
    --notes    long-ish description of a run
    --race     information about a race
    --shoes    shoes worn during a run
    """

    command
  end

  def process_commands({ :run, run }) do
    # 1. Find the .json file.
    # 2. Add the run to the .json.
    { file, workouts } = workouts_filename
                         |> read_workouts_file
                         |> initialize_workouts
                         |> initialize_runs
                         |> add_run(run)
                         |> to_json
                         |> write_workouts_file

    workouts
  end

  defp add_run({ filename, workouts }, run) do
    { filename, Map.put(workouts, :runs, [run | runs(workouts)]) }
  end

  defp initialize_runs({ filename, workouts }) do
    { filename, Map.put(workouts, :runs, runs(workouts)) }
  end

  defp initialize_workouts({ filename, "" }) do
    { filename, %{} }
  end

  defp initialize_workouts({ filename, workouts }) do
    { filename, Poison.Parser.parse!(workouts, keys: :atoms!) }
  end

  defp read_workouts_file(filename) do
    { filename, File.read!(filename) }
  end

  defp runs(workouts) do
    Map.get workouts, :runs, []
  end

  defp to_json({ filename, workouts }) do
    { filename, Poison.encode!(workouts, pretty: true) }
  end

  defp workouts_filename do
    Application.get_env(:workouts, :workouts_file)
    |> Path.expand
  end

  defp write_workouts_file(contents = { filename, workouts }) do
    File.write filename, workouts

    contents
  end
end
