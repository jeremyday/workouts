defmodule Workouts.CLI do
  @moduledoc """
  Parse the command line and dispatch to handler functions.
  """

  require Logger

  @doc """
  Process the command line arguments and run the application.

  `argv` - The command line arguments.
  """
  def run(argv) do
    argv
    |> Workouts.CLI.Parse.parse_args
    |> Workouts.CLI.Process.process_commands
  end

  @doc """
  Function that is called automatically by `escript` when the application is run from the command line.

  `argv` - The command line arguments.
  """
  def main(argv) do
    run argv
  end
end
